# E3 IOC template

This repository contains a template for a standard ESS E3 IOC startup script.

## Modules

The startup script loads the standard ESS EPICS modules using the [e3-common](https://gitlab.esss.lu.se/e3-recipes/e3-common-recipe) package:

```
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")
```

The **e3-common** package includes:
* epics-base
* require
* iocStats
* recsync
* caPutLog
* ess
* autosave

Those modules shall not be defined in the `st.cmd` file (nor in the `environment.yaml`).

The modules required by the IOC shall be added to the startup script (without version):

```
require mymodule
```

The modules to install shall also be added to the `environment.yaml` file:

```
dependencies:
  - e3-common
  - mymodule=1.0.0
```

## Databases

Local databases for this IOC should be stored in the `db` directory. Instructions to load the database should be included in the startup script.

