# require statments should only specify the module name
# The version of the module shall be defined in the environment.yaml file
# Note that all modules shall be in lowercase
## Add extra modules here
# require mymodule
#require iocstats, 3.1.16
#require streamdevice
require fug
#require tdkgen10500
require tdklambdagenesys
require sairem
require sorensen
require caenelsmagnetps


# Set EPICS environment variables
## Edit these as required for specific IOC requirements
## General IOC environment variables
epicsEnvSet("PREFIX", "IOC ISRC LEBT")
epicsEnvSet("DEVICE", "Virtual Machine Installed")
epicsEnvSet("LOCATION", "ESS.ACC_etc")
epicsEnvSet("ENGINEER", "antonisimelio@esss.se"
## Add extra environment variables here

# Load standard module startup scripts
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

## Add extra startup scripts requirements here
# iocshLoad("$(module_DIR)/module.iocsh", "MACRO=MACRO_VALUE")

# CALL to the MODULES
############################################################################################################################################

################# ISRC ########################

############################################################
######## FUG @ HCH 15k-100k [High Voltage Power Supply] ######
############################################################
#iocshLoad("$(fug_DIR)/fug.iocsh", "Ch_name=HVPS, IP_addr=172.16.60.57, P=ISrc-010:, R=ISS-HVPS:")
iocshLoad("$(fug_DIR)/fug.iocsh", "Ch_name=HVPS, IP_addr=127.0.0.1, P=ISrc-010:, R=ISS-HVPS:")


############################################################
######## FUG @HCP 35-3500 [Repeller Power Supply] ###########
############################################################
iocshLoad("$(fug_DIR)/fug.iocsh", "Ch_name=RepPS-01, IP_addr=127.0.0.1, P=ISrc-010:, R=PwrC-RepPS-01:")
#iocshLoad("$(fug_DIR)/fug.iocsh", "Ch_name=RepPS-01, IP_addr=172.16.60.52, P=ISrc-010:, R=PwrC-RepPS-01:")

iocshLoad("$(fug_DIR)/fug.iocsh", "Ch_name=RepPS-02, IP_addr=127.0.0.1, P=LEBT-010:, R=PwrC-RepPS-01:")
#iocshLoad("$(fug_DIR)/fug.iocsh", "Ch_name=RepPS-02, IP_addr=172.16.60.53, P=LEBT-010:, R=PwrC-RepPS-01:")


############################################################
############# TDK Lambda Genesys 10-500 [Coils] ############
############################################################
#iocshLoad("$(tdkgen10500_DIR)/tdkGen10500.iocsh", "connection_name=CoilsPS-01, IP_addr=10.10.1.31, secsub=ISrc-010, disdevidx=PwrC-CoilPS-01")
iocshLoad("$(tdkgen10500_DIR)/tdkGen10500.iocsh", "connection_name=CoilsPS-01, IP_addr=127.0.0.1, secsub=ISrc-010, disdevidx=PwrC-CoilPS-01")
#iocshLoad("$(tdkgen10500_DIR)/tdkGen10500.iocsh", "connection_name=CoilsPS-02, IP_addr=10.10.1.32, secsub=ISrc-010, disdevidx=PwrC-CoilPS-02")
iocshLoad("$(tdkgen10500_DIR)/tdkGen10500.iocsh", "connection_name=CoilsPS-02, IP_addr=127.0.0.1, secsub=ISrc-010, disdevidx=PwrC-CoilPS-02")
#iocshLoad("$(tdkgen10500_DIR)/tdkGen10500.iocsh", "connection_name=CoilsPS-03, IP_addr=10.10.1.33, secsub=ISrc-010, disdevidx=PwrC-CoilPS-03")
iocshLoad("$(tdkgen10500_DIR)/tdkGen10500.iocsh", "connection_name=CoilsPS-03, IP_addr=127.0.0.1, secsub=ISrc-010, disdevidx=PwrC-CoilPS-03")

############################################################
###################### Sairem @ GMP20KED [Magnetron] #########
############################################################
iocshLoad("$(sairem_DIR)/sairem_Magnetron_RfGen.iocsh", "P=ISrc-010, R=ISS-Magtr, PORT_NAME=Magtr, IP_ADDR=127.0.0.1, SLAVE_ADDR=1, MAX_PWR_W=2000")
#iocshLoad("$(sairem_DIR)/sairem_Magnetron_RfGen.iocsh", "P=ISrc-010, R=ISS-Magtr, PORT_NAME=Magtr, IP_ADDR=10.10.1.38, SLAVE_ADDR=1, MAX_PWR_W=2000")

############################################################
################# Sairem @ AI4S [ATU] ########################
############################################################
iocshLoad("$(sairem_DIR)/sairem_ATU_Tuner.iocsh", "P=ISrc-010, R=ISS-ATU, PORT_NAME=ATU, IP_ADDR=127.0.0.1, SLAVE_ADDR=0")
#iocshLoad("$(sairem_DIR)/sairem_ATU_Tuner.iocsh", "P=ISrc-010, R=ISS-ATU, PORT_NAME=ATU, IP_ADDR=10.10.1.36, SLAVE_ADDR=0")

################# LEBT ####################################

############################################################
################# Sorensen @ SGA30x501d [SOLENOIDS] ########
############################################################
iocshLoad("$(sorensen_DIR)/sorensen.iocsh", "Ch_name=SolPS-01, IP_addr=127.0.0.1, P=LEBT-010, R=PwrC-SolPS-01, Amp2Tesla_calib = 8.291E-4")
#iocshLoad("$(sorensen_DIR)/sorensen.iocsh", "Ch_name=SolPS-01, IP_addr=172.16.60.46, P=LEBT-010, R=PwrC-SolPS-01, Amp2Tesla_calib = 8.291E-4")

iocshLoad("$(sorensen_DIR)/sorensen.iocsh", "Ch_name=SolPS-02, IP_addr=127.0.0.1, P=LEBT-010, R=PwrC-SolPS-02, Amp2Tesla_calib = 8.291E-4")
#iocshLoad("$(sorensen_DIR)/sorensen.iocsh", "Ch_name=SolPS-02, IP_addr=172.16.60.47, P=LEBT-010, R=PwrC-SolPS-02, Amp2Tesla_calib = 8.291E-4")


############################################################
################# Caen @ Fast-Ps [STEERERS] ########
############################################################
#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCH-01, IP_addr=127.0.0.1, P=LEBT-010:, R=PwrC-PSCH-01:, Amp2Tesla_calib = 8.291E-4")
iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCH-01, IP_addr=172.30.4.189, P=LEBT-010:, R=PwrC-PSCH-01:, Amp2Tesla_calib = 8.291E-4")

iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCV-01, IP_addr=127.0.0.1, P=LEBT-010:, R=PwrC-PSCV-01:, Amp2Tesla_calib = 8.291E-4")
#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCV-01, IP_addr=172.30.4.189, P=LEBT-010:, R=PwrC-PSCV-01:, Amp2Tesla_calib = 8.291E-4")


iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCH-02, IP_addr=127.0.0.1, P=LEBT-010:, R=PwrC-PSCH-02:, Amp2Tesla_calib = 8.291E-4")
#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCH-02, IP_addr=172.30.4.189, P=LEBT-010:, R=PwrC-PSCH-02:, Amp2Tesla_calib = 8.291E-4")

iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCV-02, IP_addr=127.0.0.1, P=LEBT-010:, R=PwrC-PSCV-02:, Amp2Tesla_calib = 8.291E-4")
#iocshLoad("$(caenelsmagnetps_DIR)/caenelsmagnetps.iocsh", "Ch_name=SteererPSCV-02, IP_addr=172.30.4.189, P=LEBT-010:, R=PwrC-PSCV-02:, Amp2Tesla_calib = 8.291E-4")





## Define Generic Variables For Every Module
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(fug_DB):$(tdkgen10500_DB):$(sairem_DB):$(sorensen_DB):$(caenelsmagnetps_DB)")
#epicsEnvSet(STREAM_PROTOCOL_PATH, "$(fug_DB):$(tdkgen10500_DB):$(sairem_DB):$(sorensen_DB)")

#############################################################################################################################################


## Load custom databases
# cd $(E3_IOCSH_TOP)
# dbLoadRecords("db/custom_database1.db", "MACRO1=MACRO1_VALUE,...")
# dbLoadTemplate("db/custom_database2.substitutions", "MACRO1=MACRO1_VALUE,...")

# Call iocInit to start the IOC
iocInit()

## Add any post-iocInit statements here

